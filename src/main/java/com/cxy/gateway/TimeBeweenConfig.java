package com.cxy.gateway;

import lombok.Data;

import java.time.LocalTime;

/**
 * @author: huayushu luming
 * @date: 2020-12-02 22:42
 * @desc:
 **/
@Data
public class TimeBeweenConfig {
    private LocalTime start;
    private LocalTime end;
}
